var HeaderStyles = {
  header: {
    display: 'flex',
    alignItems: 'center',
    backgroundColor: '#1f1f1f'
  },
  leftheading: {
    flex: 1,
    fontSize: '1rem',
    margin: '1rem'
  },
  rightheading: {
    flex: 1,
    textAlign: 'right',
    margin: '1rem',
    color: 'white'
  }
};

var Header = React.createClass({
  render: function () {
    return (
      <header style={HeaderStyles.header}>
        <p style={HeaderStyles.leftheading}>
          <a href="https://www.nytimes.com/crosswords/index.html" target="_blank" rel="noopener noreferrer">
            Submit Your Score!
          </a>
        </p>
        <p style={HeaderStyles.rightheading}>
          <a href="https://clb-jira.atlassian.net" target="_blank" rel="noopener noreferrer">
            BACKLOG
          </a>
        </p>
      </header>
    );
  }
});
