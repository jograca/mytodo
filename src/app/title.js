var TitleStyles = {
  title: {
    display: 'flex',
    flexDirection: 'inherit',
    alignItems: 'center',
    padding: '1rem',
    backgroundColor: '#4e4e4e',
    color: 'white'
  },
  logo: {
    height: '32rem',
    backgroundColor: 'white',
    borderRadius: 'none',
    margin: '1rem'
  }
};

var Title = React.createClass({
  render: function () {
    return (
      <div style={TitleStyles.title}>
        <img style={TitleStyles.logo} src="https://raw.githubusercontent.com/jograca/comradelongbridge/master/comradelong_2.jpg"/>
      </div>
    );
  }
});
