/* eslint "react/no-find-dom-node": "off" */

'use strict';

describe('Footer', function () {
  it('should be a footer', function () {
    var footer = React.addons.TestUtils.renderIntoDocument(<Footer/>);
    var footerNode = ReactDOM.findDOMNode(footer);
    expect(footerNode.tagName).toEqual('FOOTER');
  });
});
