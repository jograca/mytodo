var SheetStyles = {
  title: {
    display: 'flex',
    flexDirection: 'inherit',
    alignItems: 'center',
    padding: '1rem',
    backgroundColor: '#4e4e4e',
    color: 'white'
  },
  h2: {
    fontWeight: 300,
    fontSize: '1.5rem'
  },
  sheet: {
    height: '32rem',
    backgroundColor: 'white',
    borderRadius: 'none',
    margin: '1rem'
  }
};

var Sheet = React.createClass({
  render: function () {
    return (
      <div style={SheetStyles.title}>
        <h2 style={SheetStyles.h2}>
          Glorious Crossword Competition!
        </h2>
        <iframe style={SheetStyles.sheet} src="https://docs.google.com/spreadsheets/d/1sd9ijOXJonTLoQXgSwh-AQ5YqhMnmaJjBHK5ZzX5oW4/pubhtml?gid=1560383919&single=true" width="800"/>
        <iframe style={SheetStyles.sheet} src="https://docs.google.com/spreadsheets/d/1sd9ijOXJonTLoQXgSwh-AQ5YqhMnmaJjBHK5ZzX5oW4/pubhtml?gid=1355614812&single=true" width="800"/>
        <iframe style={SheetStyles.sheet} src="https://docs.google.com/spreadsheets/d/1sd9ijOXJonTLoQXgSwh-AQ5YqhMnmaJjBHK5ZzX5oW4/pubhtml?gid=2130247610&single=true" width="800"/>
        <iframe style={SheetStyles.sheet} src="https://docs.google.com/spreadsheets/d/1sd9ijOXJonTLoQXgSwh-AQ5YqhMnmaJjBHK5ZzX5oW4/pubhtml?gid=410134594&single=true" width="800"/>
      </div>
    );
  }
});
