var FooterStyles = {
  footer: {
    padding: '1rem',
    fontSize: '1rem',
    backgroundColor: '#1f1f1f',
    textAlign: 'center',
    color: 'white'
  },
  github: {
    flex: 1,
    textAlign: 'right',
    margin: '1rem',
    color: 'white'
  }
};

var Footer = React.createClass({
  render: function () {
    return (
      <footer style={FooterStyles.footer}>
        <p style={FooterStyles.github}>
          <a href="https://github.com/jograca/comradelongbridge" target="_blank" rel="noopener noreferrer">
            Comrade Long Bridge on GitHub
          </a>
        </p>
      </footer>
    );
  }
});
