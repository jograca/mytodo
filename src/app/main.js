var MainStyles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100%'
  },
  main: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column'
  }
};

var Main = React.createClass({
  render: function () {
    return (
      <div style={MainStyles.container}>
        <Header/>
        <main style={MainStyles.main}>
          <Title/>
          <Sheet/>
        </main>
        <Footer/>
      </div>
    );
  }
});
