# README #

Yeoman scaffolding sample project:

http://yeoman.io/codelab/index.html

Tools used in this project:

1) Web scaffolding trough Yeoman (Fountain Webapp - http://fountainjs.io/)
2) Gulp as the build system
3) React as the JavaScript framework
4) Bower and script injection for module management
